import java.io.*;
import java.lang.*;
import java.util.*;


public class PathPlanner extends Thread {
	private int width, height; // map dimensions
	private double metersPerPixel = 0.082; // meters per pixel in the map
	private byte[] map;
	private Pose destination; // the current destination
	private Pose current; // current location
	private boolean mapDone, pathDone; // mapDone: true if PRM has been made; pathDone: true if path is ready
	private boolean foundPath; // Once pathfinding is done, set to true if we could find a path, false otherwise
	private Pose[] waypoints; // the waypoints to get from current location to destination
	private double clearDist = 0.5; // Number of meters of clear space to leave
	private int NPOINTS = 1000; // number of random points to generate
	private int NNEIGHBORS = 10;
	private Pose[] points;
	private Pose[][] edges;

	/* This function returns an array of waypoints to be visited in order
	to reach the destination */
	/* The first element is the first waypoint, and so on */
	public Pose[] getWaypoints() {
		return waypoints;
	}
	
	/* This function marks the specific edge between Pose from and to as
	untraversable */
	public void markBad(Pose from, Pose to) {
	}
	
	/* Tells the path planner to find a new set of waypoints between the
	* current position and the destination. Returns true if the given destination
	* is legal
	*/
	public boolean replan(Pose destination) {
		System.out.println("Replan to " + destination.print() + " from " + current.print());
		if (isClear(destination.getXPixel() + (int)(width/2), -1*destination.getYPixel() + (int)(width/2))) {
			foundPath = false;
			pathDone = false;
			this.destination = destination;
			return true;
		} else {
			return false;
		}
	}
	
	/* Clears out any edges marked as untraversable */
	public void reset() {
	}
	
	/* Update the current location, as returned by the localizer */
	public void updateLocation(Pose n) {
		current = n;
	}
	
	/* Returns true if a path has been calculated, false otherwise */
	public boolean pathReady() {
		return pathDone;
	}

	public boolean foundPath() {
		return foundPath;
	}

	public PathPlanner() {
		width = 1600;
		height = 500;
		try {
			map = new byte[width*height];
			FileInputStream is = new FileInputStream(new File("./3large.raw"));
			// Read in the bytes
			int offset = 0;
			int numRead = 0;
			while (offset < map.length
				&& (numRead=is.read(map, offset, map.length-offset)) >= 0) {
			            offset += numRead;
			}
		} catch (Exception e) {
		}
		points = new Pose[NPOINTS];
		edges = new Pose[NPOINTS][NNEIGHBORS];

		mapDone = pathDone = foundPath = false;
	}
	
	public void run() {
		/*
		System.out.println("can connect bogus points? " + canConnect(new Pose(4.264, -11.07, 0), new Pose(4.182, -8.446, 0)));
		canConnect(new Pose(7.954, -2.542, 0), new Pose(8.692, -5.658, 0));
		System.out.println("x = 906 y = 320, val = " + map[320*width + 906]);
		System.out.println(mToP(8.692)+(width / 2));
		try {
			Thread.sleep(1000000);
		} catch (Exception e) {}
		*/
		// Pick random points
		Random rand = new Random();
		int xpix, ypix;
		int i = 0;
		while (i < NPOINTS) {
			xpix = rand.nextInt(width);
			ypix = rand.nextInt(height);
			if (isClear(xpix, ypix)) {
				points[i] = new Pose(pToM(xpix - (int)(width / 2)), -1*pToM(ypix - (int)(height / 2)), 0);
				i++;
			}
		}

		System.out.println("finished generating points");

		int j, k, m;
		boolean canconnect;
		// Locate NNEIGHBORS closest points for every point
		for (i = 0; i < NPOINTS; i++) {
			//System.out.println("Finding edges for  point " + i);
			// initialize the edges far away
			for (j = 0; j < NNEIGHBORS; j++) {
				edges[i][j] = new Pose(1000, 1000, 0);
			}
			// find the five closest points along unobstructed edges and store them
			for (j = 0; j < NNEIGHBORS; j++) {
				Pose best = new Pose(1000, 1000, 0);

				// Check every point...
				for (k = 0; k < NPOINTS; k++) {
					// If this point is the same as the origin, ignore it, else...
					if (!samePose(points[i], points[k])) {

						// if the current point is closer than the "best" point
						if (dist(points[i], points[k]) < dist(points[i], best)) {
							if (canConnect(points[i], points[k])) {
								boolean good = true;
								// we must make sure this point hasn't already been put in the array
								for (m = 0; m < NNEIGHBORS; m++) {
									if (samePose(points[k], edges[i][m])) {
										good = false;
										break;
									}
								}
								// if it isn't already in the edges array, save it temporarily
								if (good) {
									best = points[k];
								}
							}
						}
					}
				}
				if (canConnect(points[i], best)) {
					//System.out.println("Point # " + i + ", " + points[i].print() + " adding edge to " + best.print());
					edges[i][j] = best;
				}
			}
		}
		System.out.println("done generating edges");
		mapDone = true;


		// Loop forever
		while (true) {
			// If replan hasn't been called, we keep waiting
			while (pathDone == true || destination == null) {
				try {
					Thread.sleep(50);
				} catch (Exception e) {
					// I hate this
				}
			}

			System.out.println("starting search");

			// Ok, time to find a path
			Pose start = new Pose(1000, 1000, 0); // The recorded point closest to our current position
			// Check every point...
			for (k = 0; k < NPOINTS; k++) {
					// if the current point is closer than the "best" point
					if (dist(current, points[k]) < dist(current, start)) {
						if (canConnect(current, points[k])) {
							start = points[k];
						}
					}

			}

			// Find the "end" point, closest to the destination
			Pose end = new Pose(1000, 1000, 0);
			for (k = 0; k < NPOINTS; k++) {
					// if the current point is closer than the "best" point
					if (dist(destination, points[k]) < dist(destination, end)) {
						if (canConnect(destination, points[k])) {
							end = points[k];
						}
					}
			}

			ArrayList<SearchNode> open = new ArrayList<SearchNode>();
			ArrayList<SearchNode> closed = new ArrayList<SearchNode>();
			SearchNode startnode = new SearchNode(start, end, 0);
			open.add(startnode);
			SearchNode cnode = startnode; // current node
			while (open.size() != 0) {
				SearchNode []olist = new SearchNode[open.size()];
				open.toArray(olist);
				Arrays.sort(olist);
				cnode = olist[0];
				//System.out.println("Current node = " + cnode.getPose().print());
				open.remove(open.indexOf(cnode));
				if (samePose(cnode.getPose(), end)) {
					// we're there!
					break;
				}
				closed.add(cnode);
				for (i = 0; i < NPOINTS; i++) {
					if (samePose(points[i], cnode.getPose())) {
						// for each one of this point's neighbors...
						for (j = 0; j < NNEIGHBORS; j++) {
							SearchNode n = new SearchNode(edges[i][j], end, cnode.getG() + dist(cnode.getPose(), edges[i][j]));
							if (n.getPose().getX() != 1000.0d) {
								n.setParent(cnode);
								if (closed.contains(n)) {
								} else if (open.contains(n)) {
									if (n.getG() < open.get(open.indexOf(n)).getG()) {
										open.remove(open.indexOf(n));
										open.add(n);
									}
								} else {
									open.add(n);
								}
							}
						}
						break;
					}
				}
			}
			if (cnode.getPose().equals(end)) {
				System.out.println("Found path");
				ArrayList<Pose> tmp = new ArrayList<Pose>();
				tmp.add(destination);
				tmp.add(cnode.getPose()); // end of path
				// Traverse backwards
				//System.out.println("Destination: " + destination.print() + ", nearest pt = " + end.print());
				while (cnode.getParent() != null) {
					//System.out.println(cnode.getPose().print());
					cnode = cnode.getParent();
					tmp.add(cnode.getPose());
				}
				//tmp.add(current);
				//System.out.println("Current location: " + current.print() + ", nearest pt = " + start.print());
				waypoints = new Pose[tmp.size()];
				tmp.toArray(waypoints);
				reverse(waypoints);
				foundPath = true;
				pathDone = true;
			} else {
				waypoints = new Pose[1];
				waypoints[0] = current;
				System.out.println("Couldn't find a path");
				pathDone = true;
				foundPath = false;
			}
		}
			
	}

	// Returns true if nothing obstructs the two points
	private boolean canConnect(Pose a, Pose b) {
		Pose c, d;
		if (a.getX() < b.getX()) {
			c = a;
			d = b;
		} else {
			c = b;
			d = a;
		}
		int x = c.getXPixel() + (int)(width / 2);
		int y = (-1*c.getYPixel() + (int)(height / 2));
		int xstart = x;
		int ystart = y;
		int xdest = d.getXPixel() + (int)(width / 2);
		int ydest = (-1*d.getYPixel() + (int)(height / 2));
		//System.out.printf("x = %d, y = %d, xdest = %d, ydest = %d\n", x, y, xdest, ydest);
		if (xdest - xstart != 0) {
			double slope = (float)((float)(ydest - y)/(float)(xdest - xstart));
			while (x < xdest) {
				//System.out.printf("testing %d, %d\n", x, (int)(y+(x-xstart)*slope));
				if (!isClear(x, (int)(y+(x-xstart)*slope))) {
					return false;
				}
				x++;
			}
			if (y < ydest) {
				while (y < ydest) {
					if (!isClear((int)(xstart+((y-ystart)/slope)), y))
						return false;
					y++;
				}
			} else {
				while (y > ydest) {
					if (!isClear((int)(xstart+((y-ystart)/slope)), y))
						return false;
					y--;
				}
			}
		} else {
			if (y < ydest) {
				while (y < ydest) {
					if (!isClear(x, y)) {
						return false;
					}
					y++;
				}
			} else {
				while (y > ydest) {
					if (!isClear(x, y)) {
						return false;
					}
					y--;
				}
			}
		}
		return true;
	}
	

	private boolean isClear(int xpix, int ypix) {
		int i, j, jinit;
		if (xpix - mToP(clearDist) < 0) {
			i = 0;
		} else {
			i = xpix - mToP(clearDist);
		}
		if (ypix - mToP(clearDist) < 0) {
			jinit = 0;
		} else {
			jinit = ypix - mToP(clearDist);
		}

		//System.out.println("Testing point " + xpix + "," + ypix);
		for (; i < xpix + mToP(clearDist) && i < width; i++) {
			j = jinit;
			for (; j < ypix + mToP(clearDist) && j < height; j++) {
				//System.out.println("i = " + i + ", j = " + j + ", val = " + map[j*width + i]);
				if (map[j*width + i] != 0) {
					//System.out.println("no good!");
					return false;
				}
			}
		}
		return true;
	}

	private int mToP(double m) {
		return (int)(m / metersPerPixel);
	}

	private double pToM(int p) {
		return p*metersPerPixel;
	}

	private double dist(Pose a, Pose b) {
		return Math.sqrt(Math.pow((b.getX() - a.getX()), 2) + Math.pow((b.getY() - a.getY()), 2));
	}

	private boolean samePose(Pose a, Pose b) {
		return ((a.getX() == b.getX()) && (a.getY() == b.getY()));
	}


	public void reverse(Pose[] b) {
	   int left  = 0;          // index of leftmost element
	   int right = b.length-1; // index of rightmost element
	  
	   while (left < right) {
	      // exchange the left and right elements
	      Pose temp = b[left]; 
	      b[left]  = b[right]; 
	      b[right] = temp;
	     
	      // move the bounds toward the center
	      left++;
	      right--;
	   }
	}

}
