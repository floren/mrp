public class Sonar {
		
	private double x;
	private double y;
	private double spread;
	private double angle;
	private double range;
	
	// In meters
	public static final double threshold = 5d;
	private static final double stddev = 1.0;
	
	public Sonar( double x_in, double y_in, double a, double s ) {
		setX(x_in);
		setY(y_in);
		setAngle(a);
		setSpread(s);
	}
	
	public void setRange( double r ) {
		range = r;
	}
	
	public double getRange() {
		return range;
	}
	
	// In degrees!
	public void setAngle( double a ) {
		if( a < -180 || a > 180 ) {
			System.err.println("Angles must be in degrees from -180 to 180.");
		} else {
			angle = a;
		}
	}
	
	public double getAngle() {
		return angle;
	}
	
	// In degrees!
	public void setSpread( double s ) {
		if( s < -180 || s > 180 ) {
			System.err.println("Spread must be in degrees from -180 to 180.");
		} else {
			spread = s;
		}
	}
	
	public double getSpread() {
		return spread;
	}
	
	public void setX( double x_in ) {
		x = x_in;
	}
	
	public void setY( double y_in ) {
		y = y_in;
	}
	
	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}
	
	public double getR() {
		return Math.sqrt( getX()*getX() + getY()*getY() );
	}
	
	public String toString() {
		String s = "" + range;
		return s;
	}
	
	// Returns degrees!
	public double getTheta() {
		// We use atan2 because it handles special cases
		// atan2 takes y,x as parameters (note the order!!)
		return Math.toDegrees(Math.atan2( getY(),getX() ));
	}
	
	// Using Gaussian + Delta PDF return prob
	public double getProb(double x) {
		
		if( x >= threshold && range != threshold) {
			// Chance of getting threshold reading
			// Delta portion
			return 0.1;
		} else {
			
			// Gaussian portion
			
			double sigma = stddev*stddev;
			return (1 / Math.sqrt( 2 * Math.PI * sigma )) * Math.exp( -1 * ((x-range)*(x-range))/(2*sigma) );
			
			
		}
		
	}
	
}
