import java.util.LinkedList;
import java.util.Queue;
import java.util.ArrayList;
public class RTOA extends Thread{
PathPlanner p;
float[] ranges=new float[8];
Pose[] coor;
Pose pose;
Pose last_coor;
speed_direction sp_dir;
Queue<Pose> q=new LinkedList<Pose>();
boolean turn;
boolean slope_check;
boolean is_ready;
long time;
ArrayList<Pose> visitMe; // list of destinations to go to (from file)
Pose currentDestination; // the point to call with replan

RTOA(PathPlanner p, ArrayList<Pose> visit)
{
this.p=p;
visitMe = visit;
sp_dir=new speed_direction();
    for(int i=0;i<8;i++)
    {
        ranges[i]=0.0f;    
    }
pose=new Pose(0,0,0);
last_coor=new Pose(0,0,0);
coor=new Pose[100];
}
void update_sonars(float[] ranges,Pose pose)
{
    this.ranges=ranges;
    this.pose=pose;
    
	this.is_ready = true;
        //System.out.println(this.ranges[0]);
    
}
void Goto(Pose position)
{
   time=System.currentTimeMillis();
   System.out.println("Inside goto");
    float x=(float)position.getX();
    float y=(float)position.getY();
 System.out.println("goinng towards :X= "+x+" y= "+y);
	   turn=true;
    float speed=0.0f,turnrate=0.0f;
     slope_check=true;
    while (turn) {
        float rotatex=0,rotatey=0;
 
        
        float angle2=0;
        //System.out.println(position.getX()+" "+position.getY());
        //System.out.println("rages= "+ranges[0]+" "+ranges[1]);
        //sp_dir.setSpeed(0.1f,0.0f);
        double distance,angle;
        //Calculate position with respect to desired location from current location.
        rotatey=y-(float)pose.getY();
        rotatex=x-(float)pose.getX();
        //Calculating angle to rotate to move towards goal location.
        angle=(float)Math.atan2(rotatey,rotatex);
    double deg=angle*180/3.14159;
        //double cur_deg=motor.getData().getPos().getPa()*180/3.14159;
        double cur_deg=pose.getTheta();
	//System.out.printf("current degrees = %f, target degrees = %f\n", cur_deg, deg);
	if (deg < 0) {
		deg += 360;
	}
	if (cur_deg < 0) {
		cur_deg += 360;
	}
	double temp_cur_deg = cur_deg;
	double temp_deg = deg;
	if (cur_deg < 0) {
		temp_cur_deg += 360;
	}
	if (deg < 0) {
		temp_deg += 360;
	}

	//System.out.printf("current degrees = %f, target degrees = %f\n", cur_deg, deg);
    double rotation=1.0d;
	if (temp_deg - temp_cur_deg > 180 || (temp_deg - temp_cur_deg < 0 && temp_deg - temp_cur_deg > -180)) {
		rotation = -1.0d;
	}
/*
    if((deg-cur_deg)<0 )
    {
         rotation =-1.0d;
    } 
*/
       
        //Read data from the robot.
        
     
       // System.out.println("Expected angle is := "+deg+" Current angle is :="+pose.getTheta());
        /*if(deg<0)
        {
            deg=360+deg;
        }
	*/
        //Check if we have rotated in desired manner and conditions take into consideration some error.
    long currtime=System.currentTimeMillis();
    rotatex=0;rotatey=0;
        //Setting flag for moving to goal position. 
        //Here the formula time=distance/speed is used to notify that we have reached at desired location.
        boolean move=true;
        //Getting position with respect to goal and current location of robot. 
        rotatey=y-(float)pose.getY();
        rotatex=x-(float)pose.getX();
        //System.out.println("x= "+pose.getX()+" y="+pose.getY());
        //Calculating distance w.r.t goal and current location 
        double distance2 = Math.sqrt(rotatex*rotatex+rotatey*rotatey);
        //Take snapshot of current time.
       
    //Taking snapshot of the current time and dividing by 1000 to round get time is seconds.
        
        double check=(double)(currtime-time)/1000;
        //Calculating distance over speed
        double distoverspeed=distance2/0.2; 
        rotatey=y-(float)pose.getY();
        rotatex=x-(float)pose.getX();
        distance2 = Math.sqrt(rotatex*rotatex+rotatey*rotatey);
      //  System.out.println("Distance from goal is ="+distance2);
       // System.out.println("Check= "+check+" dist= "+distoverspeed);
     float dist_from_wall=Math.min(ranges[3],ranges[4]);
	float right_dist=Math.min(ranges[7],ranges[6]);
	float left_dist=Math.min(ranges[0],ranges[1]);
      //  System.out.println("dist from wall= "+dist_from_wall);
        if((dist_from_wall<0.4f && speed!=0.0f) || (right_dist<0.2 || left_dist<0.25)){
            obstacle_avoidance(position);
           

        }
    
        if((cur_deg>=(deg-5) && cur_deg<=(deg+5)) || cur_deg==deg) {
        //Setting flag as false so that now robot can move in towards the goal.
       			 if(distance2<1)
    			{
        			//System.out.println("speed");
        			speed=0.1f;
    			}
			else{
               			speed=0.1f;
        }
	
	
	
        turnrate=0.0f;
         //sp_dir.setSpeed(0.2f, 0.0f);
        
         //sp_dir.setSpeed(0.2f, 0.0f);
        }else{
	speed=0.0f;
        //Just turning the robot at calculated angle speed=0.1f;
	  if(Math.abs(deg-cur_deg)<30)
	{
        turnrate=(float)rotation*.11f;
            //sp_dir.setSpeed(0.0f,(float).11);
	}
	else
	{
		turnrate=(float)rotation*.11f;

	}
        
    }
    	
 	double front_dist=Math.min(ranges[3],ranges[4]);


     //System.out.println("speed= "+speed);
     // System.out.println("Distance ="+distance2+" speed= "+speed);
    if(distance2<0.15){turn=false;   } sp_dir.setSpeed(speed,turnrate);
        }
   
        
       sp_dir.setSpeed(0.0f,0.0f);
    
}
speed_direction getspeed()
{
    return sp_dir;
}

void obstacle_avoidance(Pose p)
{
	System.out.println("in obstacle avoidance");
   float x=(float)p.getX();
    float y=(float)p.getY();
    long turn_time=System.currentTimeMillis();
      float Def_speed=0.20f,Def_angle=0.15f; //Default speed and Default angle if robot deviates.
     float right_dist,front_dist,left_dist; //Distance from the right hand side wall and Distance from the front of the wall
     float dist_from_wall=0.5f;            //Maintain distance from wall of 0.5
    long turn_sec;
    float max=0.4f,min=0.4f;
    float turnRate=Def_angle, speed=Def_speed;
    
    double slope;
    if(pose.getX()-currentDestination.getX()!=0){
    slope=(pose.getY()-currentDestination.getY())/(pose.getX()-currentDestination.getX());
    }
    else
    {
    slope=999999;
    }

    boolean flag1=true;
    boolean flag_turning;
	float right_max= Math.min(ranges[7],ranges[6]);
float left_max=Math.min(ranges[0],ranges[1]);
System.out.println("right max= "+right_max+" left_max= "+left_max);
if(left_max>right_max)
{
	
	System.out.println("follow right");
	flag_turning=true;
}
else{
	System.out.println("follow left");
	flag_turning=false;
}
boolean turn_change=true;
    while(flag1!=false){
    //Take note of right wall distance from the robot
    
    right_dist=Math.min(ranges[7],ranges[6]);
    left_dist=Math.min(ranges[0],ranges[1]);
    //Take a note of front wall distance.
    front_dist=Math.min(ranges[3],ranges[4]);
  long system_time=System.currentTimeMillis();
    //Calculate the minimum and maximum distance which robot has traversed from the right wall.
 if((system_time-time)>1000){
	if(turn_change==true)
	{
		if(flag_turning!=true)
		{
			flag_turning=true;
		}
		else
		{
			flag_turning=false;
		}
	turn_change=false;
	}
	
	}
 if(flag_turning!=false){
   
    //Close to wall in Front then turn and decrease the speed so robot does not drift away
    if(front_dist < 0.3f)
    {
        turnRate=Def_angle*3;
        speed=0.0f;
    }
    //Close to wall on right hand side move away from the wall and also decrease the speed
    else if(right_dist < (dist_from_wall - 0.05))
    {
        
	   
		turnRate=Def_angle*2;
        speed=Def_speed/2;
 
 
    }
    //Away from wall turn accordingly so that robot moves towards the wall
    else if(right_dist > (dist_from_wall + 0.05))
    {
 
        turnRate=-Def_angle*2;
        speed=Def_speed/2;
    }
    //If distance is with the range of 0.45 and 0.55 go straight
    else
    {
        turnRate=0;
        speed=Def_speed;
    }
    }else
	{
	
 //System.out.println("following left");
    //Close to wall in Front then turn and decrease the speed so robot does not drift away
    if(front_dist < 0.3f)
    {
        turnRate=-Def_angle*3;
        speed=0.0f;
    }
    //Close to wall on right hand side move away from the wall and also decrease the speed
    else if(left_dist < (dist_from_wall - 0.05))
    {
        
		
	   turnRate=-Def_angle*2;
        speed=Def_speed/2;
 
 
    }
    //Away from wall turn accordingly so that robot moves towards the wall
    else if(left_dist > (dist_from_wall + 0.05))
    {
 
        turnRate=Def_angle*2;
        speed=Def_speed/2;
    }
    //If distance is with the range of 0.45 and 0.55 go straight
    else
    {
        turnRate=0;
        speed=Def_speed;
    }
	}
    //Print the right wall distance along with Max and Min distance from the wall.
    //System.out.println("Right side distance:"+right_dist);
    //System.out.println("Max= "+max+"Min "+min);
    double check=0.0;
    if(pose.getX()-currentDestination.getX()!=0){
    check=(pose.getY()-currentDestination.getY())/(pose.getX()-currentDestination.getX());
    }
    else
    {
        if(pose.getX()==currentDestination.getX())
        {
            check=999999;
        }
    }
	float rotatey=y-(float)pose.getY();
        float rotatex=x-(float)pose.getX();
        //Calculating angle to rotate to move towards goal location.
        float angle=(float)Math.atan2(rotatey,rotatex);
    double deg=angle*180/3.14159;
        //double cur_deg=motor.getData().getPos().getPa()*180/3.14159;
        double cur_deg=pose.getTheta();
   // System.out.println("Expected slope= "+slope+" Current slope ="+check);
    long curr_time=System.currentTimeMillis();
	if(curr_time-time>5000){
	//System.out.println(cur_deg+" "+deg);
if(((cur_deg>=(deg-5) && cur_deg<=(deg+5)) || cur_deg==deg) && front_dist>0.5 && right_dist > 0.3 && left_dist >0.3){
	System.out.println("can see current point");
	flag1=false;}}
    if((curr_time-time)>10000)
    {
        if(Math.abs(check-slope)<0.05 && slope_check!=false){
	slope_check=false;
	System.out.println("Slope matched");
        flag1=false;    
        }
    }
        if(curr_time-time>5000){
	Pose pnext=q.peek();
	if(q.peek()!=null){
	float xnext=(float)pnext.getX();
	float ynext=(float)pnext.getY();
	float rotatenextx=xnext-(float)pose.getX();
	float rotatenexty=ynext-(float)pose.getY();
	float nextangle=(float)Math.atan2(rotatenexty,rotatenextx);
	double exp_next_angle=nextangle*180/3.14159;
	 //double cur_deg=pose.getTheta();
	if((cur_deg>=(exp_next_angle-5) && cur_deg<=(exp_next_angle+5)) || cur_deg==deg &&front_dist>1.0){
	System.out.println("going to next point");
	flag1=false;
	turn=false;}
	}
	
	}
	//System.out.println("current time= "+(curr_time-time));
	if(curr_time-time>10000){
		//this.p.replan(new Pose(pose.getX(), pose.getY(), pose.getTheta()));
		this.p.replan(currentDestination);
		 System.out.println("waiting for path to be ready");
	while (!this.p.pathReady()) {
		//System.out.println("wait");
		try {
			Thread.sleep(100);
		} catch (Exception e) {}
	} 
	System.out.println("path is ready");
	if (this.p.foundPath()) {
		
	while(q.peek()!=null){
	q.remove();
		}
		System.out.println("foundPath returned true");
	// getWaypoints gives you the list of waypoints. Traverse them in order.
		coor = this.p.getWaypoints();
		System.out.println("got waypoints");
		for (int i = 0; i < coor.length; i++) {
			
			q.add(coor[i]);
		}
	}
	flag1=false; 
	turn=false;
	}
    //Set the speed and turnrate accordingly
    sp_dir.setSpeed(speed, turnRate);
    
    
}
sp_dir.setSpeed(0.0f,0.0f);

}
@Override
public void run() {
	while (!is_ready) {
		try {
			Thread.sleep(100);
		} catch (Exception e) {}
	}
	System.out.println("RTOA started");
	for (int k = 0; k < visitMe.size(); k++) {
		currentDestination = visitMe.get(k);
		p.replan(currentDestination);
		System.out.println("waiting for path to be ready");
		while (!p.pathReady()) {
			//System.out.println("wait");
			try {
				Thread.sleep(100);
			} catch (Exception e) {}
		} 
		System.out.println("path is ready");
		if (p.foundPath()) {
			System.out.println("foundPath returned true");
		// getWaypoints gives you the list of waypoints. Traverse them in order.
			coor = p.getWaypoints();
			System.out.println("got waypoints");
			for (int i = 0; i < coor.length; i++) {
				
				q.add(coor[i]);
			}
		}
	            while(q.peek()!=null){
	            Goto(q.remove());
	            }
			System.out.println("We have reached our destination,"+ currentDestination.print());
	}
        
// TODO Auto-generated method stub
}

}
