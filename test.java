import javaclient2.PlayerClient;
import javaclient2.Position2DInterface;
import javaclient2.SonarInterface;
import javaclient2.structures.PlayerConstants;
import javaclient2.structures.sonar.PlayerSonarData;
import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.StringTokenizer;
public class test {

	public static void main(String args[])
	{
		int port = 6665;
		String server = "localhost";

		if (args.length == 2) {
			server = args[0];
			port = Integer.parseInt(args[1]);
		}

		PlayerClient robot = new PlayerClient(server, port);
		SonarInterface sonar = robot.requestInterfaceSonar(0, 
		PlayerConstants.PLAYER_OPEN_MODE);
		Position2DInterface motor = robot.requestInterfacePosition2D(0, 
		PlayerConstants.PLAYER_OPEN_MODE);
		PathPlanner p=new PathPlanner();
		RTOA rtoa=new RTOA(p);
		rtoa.start();
		speed_direction sp_dir=new speed_direction();
		/*float[] ranges=new float[8];
		for(int i=0;i<8;i++)
		{
		ranges[i]=0.0f;	
		}*/
		while(true)
		{
			robot.readAll();
			
			if (sonar.isDataReady()) {
				PlayerSonarData sonarData = sonar.getData();
				float[]  ranges = sonarData.getRanges();
				 if (ranges.length == 0)
					continue;
				
				robot.readAll();
				float x=motor.getX();
				float y=motor.getY();
				float deg=motor.getYaw();
				
				Pose pose=new Pose(x,y,deg);
				rtoa.update_sonars(ranges,pose);
				sp_dir=rtoa.getspeed();
				motor.setSpeed(sp_dir.getSpeed(),sp_dir.getTurnRate());
				// for some reason the first read gets nothing...
				
			}
		}
	}
		

}
