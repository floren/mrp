/* This class demonstrates how you can use the path planner */

public class TestPP {
	public static void main(String[] args) {
		PathPlanner pp = new PathPlanner();
		pp.start();
		pp.updateLocation(new Pose(-30, -9, 0)); // give it your current position
		pp.replan(new Pose(-30, 12.7, 0)); // Have it start finding a path to the given location
		while (!pp.pathReady()) {} // Wait until the pathfinding is done
		if (pp.foundPath()) {
			Pose []wp = pp.getWaypoints();
			for (int i = 0; i < wp.length; i++) {
				System.out.println(wp[i].print());
			}
		}
		System.out.println("");

		pp.updateLocation(new Pose(-30, 12.7, 0));
		pp.replan(new Pose(-30, -9, 0));
		while (!pp.pathReady()) {}
		if (pp.foundPath()) {
			Pose[] wp = pp.getWaypoints();
			for (int i = 0; i < wp.length; i++) {
				System.out.println(wp[i].print());
			}
		}
	}
}
