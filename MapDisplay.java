import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.*;
import java.awt.geom.Point2D;

/**
 * Modified GridMap for displaying Localizing Map
 *
 * @author Xander Fiss
 */
public class MapDisplay extends JFrame {

	private byte map[];
	private BufferedImage theMap;
	private int imwidth, imheight;
	private int midgray = (0xff << 24) | (180 << 16) | (180 << 8) | (180);
	private double scale = 0.082; // meters per pixel in the map
	
	LinkedList<RoboStat> prevPoints;
	
	/**
	 * Construct a map image of given size and resolution.
	 * Map's <b>center</b> will be at (0,0) and coordinates right-handed.
	 * @param width map width in meters
	 * @param height map height in meters
	 * @param mpp Resolution in meters per pixel
	 */
	public MapDisplay(byte[] m, int width, int height) {
		
		map = m;
		imheight = height;
		imwidth = width;
		
		resetMapDisplay();

	}
	
	public void resetMapDisplay() {
		
		theMap = new BufferedImage(imwidth,imheight,BufferedImage.TYPE_INT_ARGB);
		
		for (int x = 0; x < imwidth; x++)
			for (int y = 0; y < imheight; y++)
				theMap.setRGB(x,y,grayToRGB(~map[y*imwidth + x]));

		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setPreferredSize(new Dimension(imwidth, imheight));

		MapPanel mp = new MapPanel();
		add(mp);
		
	}
	
	public boolean occupied( Pose p ) {
		return occupied(p.getX(),p.getY());
	}
	
	public boolean occupied( double xd, double yd ) {
		int x = robotToMapX(xd);
		int y = robotToMapY(yd);
		return occupied(x,y);
	}
	
	public boolean occupied( int x, int y ) {
		try {
			if( theMap.getRGB(x,y) == 0xFF000000 ) {
				return true;
			} else {
				return false;
			}
		} catch ( ArrayIndexOutOfBoundsException e ) {
			return true;
		}

	}
	
	public void drawPointCloud( LinkedList<RoboStat> cloud ) {
		prevPoints = new LinkedList<RoboStat>(cloud);
		for( RoboStat r: cloud ) {
			// p is in meters
			int x = robotToMapX(r.getPose().getX());
			int y = robotToMapY(r.getPose().getY());
			//System.out.println("New cloud point @ " + p + ".");
			theMap.setRGB( x, y, 0xFFFF0000 );
		}
	}
	
	public void erasePreviousPoints() {
		for( RoboStat r: prevPoints ) {
			int x = robotToMapX(r.getPose().getX());
			int y = robotToMapY(r.getPose().getY());
			theMap.setRGB( x, y, 0xFFFFFFFF );
		}
	}
	
	public int grayToRGB(int value) {
		return (0xff << 24) | (value << 16) | (value << 8) | value;
	}
	
	public int robotToMapScale(double d) {
		return (int)(d/scale);
	}
	
	public double mapToRobotScale(double d) {
		return ((double)d*scale);
	}
	
	public int robotToMapX(double x) {
		return (int)(x/scale + imwidth/2);
	}
	
	// flip y to go from right-handed world to left-handed image
	public int robotToMapY(double y) {
		return (int)(imheight/2 - y/scale);
	}
	
	/**
	 * Update the map.
	 * @param x X location to update (global coords, in meters)
	 * @param y Y location to update (global coords, in meters)
	 * @param value New map value (0->255)
	 */
	void setGrayFromRobotXY(double x, double y, int value) {
		if (value < 0 || value > 255)
			return;
		int imx = robotToMapX(x);
		int imy = robotToMapY(y);
		setGray(imx,imy,value);
	}
	
	/**
	 * Update the map using Map Coordinates.
	 * @param x X location to update (map coords, in pixels)
	 * @param y Y location to update (map coords, in pixels)
	 * @param value New map value (0->255)
	 */
	void setGray(int imx, int imy, int value) {
		if (imx >= 0 && imx < imwidth && imy >= 0 && imy < imheight)
			theMap.setRGB(imx,imy,grayToRGB(value));
	}
	
	/**
	 * Update the map by increasing or decreasing the current gray value
	 * @param x X location to update (global coords, in meters)
	 * @param y Y location to update (global coords, in meters)
	 * @param amount Amount to change map value
	 * (will not allow setting of values < 0 || values > 255)
	 */
	void changeGrayVal(int x, int y, int amount) {
		
		int value;
		
		if (x >= 0 && x < imwidth && y >= 0 && y < imheight) {
			value = theMap.getRGB(x,y);
		} else {
			return;
		}
		
		value &= 0x000000FF;
		value += amount;
		
		if( value > 255 ) {
			value = 255;
		} else if( value < 0 ) {
			value = 0;
		} 
		
		setGray(x,y,value);
	
	}
	
	class MapPanel extends JPanel {

		protected void paintComponent(Graphics g) {
			g.drawImage(theMap,0,0,null);
		}
		public Dimension getPreferredSize() {
			return new Dimension(imwidth,imheight);
		}
	}
}
