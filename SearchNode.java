public class SearchNode implements Comparable {

	private Pose p;
	private double g, h, f;
	private SearchNode parent;

	public int compareTo(Object other) {
		double otherf = ((SearchNode) other).getF();  
		return (int)(10000*(f - otherf));
	}

	public boolean equals(Object other) {
		Pose otherP = ((SearchNode) other).getPose();
		return otherP.equals(p);
	}

	public SearchNode( Pose me, Pose destination, double g ) {
		p = me;
		this.g = g;
		h = dist(me, destination);
		f = g + h;
	}

	public void setParent( SearchNode parent ) {
		this.parent = parent;
	}

	public void setG( double g ) {
		this.g = g;
		f = g + h;
	}

	public Pose getPose() {
		return p;
	}

	public double getG() {
		return g;
	}

	public double getH() {
		return h;
	}

	public double getF() {
		return f;
	}

	public SearchNode getParent() {
		return parent;
	}

	private double dist(Pose a, Pose b) {
		return Math.sqrt(Math.pow((b.getX() - a.getX()), 2) + Math.pow((b.getY() - a.getY()), 2));
	}
}
