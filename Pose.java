/** 
 * X Y Theta triplet
 * @author Xander Fiss
 */


public class Pose {
	
	private double x;
	private double y;
	private double theta;

	private double metersPerPixel = 0.082; // meters per pixel on given map
	
	public Pose( double x_in, double y_in, double theta_in ) {
		setX(x_in);
		setY(y_in);
		setTheta(theta_in);
	}
	
	public Pose() {
		this(0d,0d,0d);
	}
	
	public void setX( double x_in ) {
		x = x_in;
	}
	
	public void setY( double y_in ) {
		y = y_in;
	}
	
	public void setTheta( double theta_in ) {
		theta = theta_in;
	}
	
	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}
	
	public double getTheta() {
		return theta;
	}
	
	public int getXPixel() {
		return (int)(x / metersPerPixel);
	}

	public int getYPixel() {
		return (int)(y / metersPerPixel);
	}

	public String print() {
		return "("+x+","+y+","+theta+")";
	}
	
	public String toString() {
		return print();
	}
	
	public boolean equals(Object other) {
		double otherX = ((Pose) other).getX();
		double otherY = ((Pose) other).getY();
		double otherTheta = ((Pose) other).getTheta();
		return (otherX == x && otherY == y && otherTheta == theta);
	}
	
	public static Pose sumPose( Pose a, Pose d ) {
		//return new Pose( p1.getX() + p2.getX(), p1.getY() + p2.getY(), LocalizeHelper.validAngle(p1.getTheta() + p2.getTheta()) ); 
		
		double z = Math.sqrt(d.getX()*d.getX() + d.getY()*d.getY());
		double t = Math.toRadians(a.getTheta() + d.getTheta());
		
		Pose r = new Pose();
		r.setX( a.getX() + z*Math.cos( t ) );
		r.setY( a.getY() + z*Math.sin( t ) );		
		//r.setTheta( LocalizeHelper.validAngle(a.getTheta() + d.getTheta()));
		r.setTheta( a.getTheta() + d.getTheta() );
		
		return r;
		
	}
	
	// P1 - P2
	public static Pose diffPose( Pose p1, Pose p2) {
		//return new Pose( p1.getX() - p2.getX(), p1.getY() - p2.getY(), LocalizeHelper.validAngle(p1.getTheta() - p2.getTheta()) ); 
		return new Pose( p1.getX() - p2.getX(), p1.getY() - p2.getY(), p1.getTheta() - p2.getTheta() ); 
	}
	
}
