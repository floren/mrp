import java.io.*;
import java.lang.*;
import java.util.*;

/** 
 * Helper class to make Localizer easier to read and understand
 * This does all of the dirty work
 */
public class LocalizeHelper {
	
	// Robot Status
	LinkedList<RoboStat> roboStats;
	private boolean isReady = false;
	RoboStat mostLikely;
	
	// Point cloud
	private LinkedList<Pose> pointCloud;
	private int numPointsPerGeneratedLocation = 1500;
	private double stdDevSpread = 0.12;
	private double angleResolution = 90d;
	private double distanceResolution = 0.1;
	private double probThreshold = 2.8;
	public boolean pointCloudCreated = false;
	private Pose totalDistanceMoved = new Pose(0d,0d,0d);
	
	// Stuff for the map
	private int width = 1600;
	private int height = 500;
	private byte[] map;
	private MapDisplay mapDisplay;
	
	//private float[] ranges = {1.29f, 1.59f, 2.34f, 5.00f, 5.00f, 3.21f, 2.20f, 1.79f};
	private float[] ranges;

	
	// This is taken directly from pioneer.inc
	private static final double spose[][] = { 
		{0.075,0.130,90.0},
		{0.115,0.115,50.0},
		{0.150,0.080,30.0},
		{0.170,0.025,10.0},
		{0.170,-0.025,-10.0},
		{0.150,-0.080,-30.0},
		{0.115,-0.115,-50.0},
		{0.075,-0.130,-90.0}
	};
	
	// This is taken directly from project.world
	private static final Pose startingLocations[] = {
		new Pose(-15.5,  12.0,   0.0),
		new Pose(-16.5,  12.0, 180.0),
		new Pose( -5.0, -10.5,   0.0),
		new Pose(  7.5,   1.0,  90.0),
		new Pose(-48.0,  12.0,  90.0),
		new Pose(-48.0, -10.5, -90.0),
		new Pose(  7.5,  -5.0,  90.0),
		new Pose(  0.0,  -7.0, -90.0)
	};
	
	/** 
	 * LocalizeHelper Constructor
	 */
	public LocalizeHelper() {
		
		// Build the map from input file
		buildMap();
		
		// Make a new MapDisplay for visualization
		// This also translates to proper coordinates
		mapDisplay = new MapDisplay(map,width,height);
			
	}
	
	/** 
	 * Reads in raw data to produce a map, where 255 is an obstacle and 0 is empty
	 * The data is stored as:
	 * byte [] map
	 * 
	 */
	public void buildMap() {
		try {
			map = new byte[width*height];
			FileInputStream is = new FileInputStream(new File("./3large.raw"));
			// Read in the bytes
			int offset = 0;
			int numRead = 0;
			while (offset < map.length && (numRead=is.read(map, offset, map.length-offset)) >= 0) {
			            offset += numRead;
			}
		} catch (Exception e) {
			System.err.println("Unable to build map.");
		}
	}
	
	/**
	 * Shows the Map Visualization UI
	 */
	public void displayMap() {
		mapDisplay.resetMapDisplay();
		mapDisplay.drawPointCloud(roboStats);
		mapDisplay.pack(); 
		mapDisplay.setVisible(true);
	}
	
	
	public void updateMap() {
		mapDisplay.erasePreviousPoints();
		mapDisplay.drawPointCloud(roboStats);
	}
	
	
	
	
	
	
	
	
	/** 
	 * This uses the listed possible starting positions
	 * and generates a list of points to test
	 */
	public void buildRoboStats() {
		Random r = new Random();
		roboStats = new LinkedList<RoboStat>();
		
		double x,y;
		
		for( Pose p: startingLocations ) {
			// Add the initial starting position
			roboStats.add(new RoboStat(p));
			
			// Add some randomly generated points
			/*
			for( int i = 0; i < numPointsPerStartingLocation; i++ ) {
				x = p.getX() + stdDevSpread*r.nextGaussian();
				y = p.getY() + stdDevSpread*r.nextGaussian();
				if( !mapDisplay.occupied(x,y) )
					for( double theta = 0d; theta < 360d; theta += angleResolution ) 
						roboStats.add(new RoboStat(new Pose(x,y,theta)));
			}
			*/
			
		}
		
		pointCloudCreated = true;
		
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Sets the internal sonar ranges variable
	 */
	public void setRanges( float r[] ) {
		ranges = r;
	}
	
	
	
	
	
	
	
	
	/** 
	 * Adds in odometrically measured movement shift to point cloud points
	 */
	public void updateRoboStats( Pose difference ) {
		
		totalDistanceMoved = Pose.sumPose( totalDistanceMoved, difference );
		Pose newPose;
		
		for( RoboStat r: roboStats ) {
			newPose = Pose.sumPose(r.getPose(),difference);
			if( !mapDisplay.occupied( newPose ) ) {
				r.setPose( newPose );
			}
		}
		
	}
	
	
	
	
	
	/**
	 * Adds more robo stats to account for sucky odometry
	 */
	public void makeNewRoboStats( Pose d ) {
		
		// Generate some random points in POLAR COORDINATES
		// Variance in RADIUS and THETA
		// Convert these points to X Y THETA
		
		Pose newPose;
		Pose noisyDiff;
		Random rand = new Random();
		double angle;
		double radius = 0d;
		double theta = 0d;
		double r_alpha = 10d;
		double t_alpha = 1d;
		
		LinkedList<RoboStat> newRoboStats = new LinkedList<RoboStat>();
		for( RoboStat r: roboStats ) {
			for( int i = 0; i < numPointsPerGeneratedLocation; i++ ) {
				
				radius = r_alpha*rand.nextGaussian()*(distanceResolution);
				theta = t_alpha*rand.nextGaussian()*(angleResolution);
				
				angle = Math.atan2(d.getY(),d.getX());
				radius += Math.sqrt(d.getX()*d.getX() + d.getY()*d.getY());
				theta += d.getTheta();
				
				noisyDiff = new Pose(radius*Math.cos(angle),radius*Math.sin(angle),theta);
				
				/*
				System.out.println("X:" + x + " " + xr + " " + xd);
				System.out.println("Y:" + y + " " + yr + " " + yd);
				System.out.println("T:" + t + " " + tr + " " + td);
				System.out.println();
				*/
								
				newPose = Pose.sumPose( r.getPose(), noisyDiff );
				//System.out.println(newPose);
				if( !mapDisplay.occupied( newPose ) ) {
					newRoboStats.add( new RoboStat( newPose ) );
				}
					
				
			}	
		}
		
		roboStats.addAll(newRoboStats);
	}
	
	
	
	
	
	
	/** 
	 * Go to each point in the point cloud
	 * Build a RoboStat object
	 * Compare the current sonars with the sonars repo
	 * Generate a probability and store int the RoboStat object
	 * 
	 */
	public void traceRoboStats( float ranges[] ) {
		
		setRanges(ranges);
		for( RoboStat r: roboStats ) {
			r.setSonars( raytraceSonars( r.getPose() ) );
			r.updateProb(ranges);
		}

	}
	
	
	
	
	
	
	
	


	/** 
	 * Get rid of robostats below a certain threshold
	 */
	public void pruneRoboStats() {

		// Incorporate into the Sonar probs the prob related to odometry
		// (We don't want to get too far away)
		//for(RoboStat r: roboStats)
		
		
		Collections.sort(roboStats);
		int size = roboStats.size();
		int prunesize = size / 10;
		if( prunesize == 0 && size > 0) {
			prunesize = 1;
		} else if( prunesize > 40 ) {
			prunesize = 40;
		}
		
		
		LinkedList<RoboStat> newList = new LinkedList<RoboStat>();
		RoboStat popped;
		for( int i = 0; i < prunesize; i++) {
			popped = roboStats.removeFirst();
			if( popped.getCurrentProb() > probThreshold || newList.size() == 0)
				newList.add( popped );
		}
		
		roboStats = new LinkedList<RoboStat>( newList );
		
		if( roboStats.size() <= 5 ) {
			isReady = true;
			mostLikely = roboStats.get(0);
		}
			
		
		//System.out.println(roboStats.size() + " Possible Locations");
		//for(RoboStat r: roboStats)
		//	System.out.println(r.getCurrentProb() + " @ " + r.getPose());
	}
	
	
	public boolean isReady() {
		return isReady;
	}
	
	public Pose getLocation() {
		return mostLikely.getPose();
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/** 
	 * Create the list of sonar objects
	 */
	public ArrayList<Sonar> buildSonars() {
		ArrayList<Sonar> sonars = new ArrayList<Sonar>();
		for(int i = 0; i < 8; i++)
			sonars.add(new Sonar( spose[i][0], spose[i][1], spose[i][2], 15 ));
		return sonars;	
	}
	
	
	/** 
	 * Ensures that we are using trigonometrically appropriate angles
	 */
	public static double validAngle(double angle) {
		if( angle > 180 ) {
			return (360 - angle);
		} else if( angle < -180 ) {
			return (360 + angle);
		} else {
			return angle;
		}
	}

	/** 
	 * Does euclidean distance formula
	 * @param X1, X2, Y1, Y2
	 * @return distance
	 */
	public static double Distance2D (double X1, double X2, double Y1, double Y2) {
		return Math.sqrt( Math.pow((X2-X1),2.0) + Math.pow((Y2-Y1),2.0) );
	}

	/** 
	 * Given a Pose corresponding to the sonar transducer
	 * sweep out until we either intersect with an obstacle or
	 * we hit 5m (or for physical hardware, ~2.4m b/c of carpet)
	 * If we are at the arc that intersects an obstacle
	 * make sure to check every point in the arc for intersection 
	 * and return the closest
	 * 
	 * @param Pose p				// Robot pose (from point cloud)
	 * @return ArrayList<Sonar> sonars	// Distances to obstacles (or not)
	 */
	public ArrayList<Sonar> raytraceSonars( Pose p ) {
		
		ArrayList<Sonar> sonars = buildSonars();
		
		double robo_x = p.getX();
		double robo_y = p.getY();
		double robo_yaw = p.getTheta();
		double phi;
		double x;
		double y;
		double theta;
		
		// Map Space bounding box xy pairs
		int x1,x2,x3,y1,y2,y3;
		// Angles including spread for xy2, xy3
		double t1,t2,t3;
		// Bounding box max and min
		int maxx,maxy,minx,miny;
		// Bounding box wiggle room
		int d;
		
		double r,t;
		
		// Give the sonar object its current sensed range
		//for(int i = 0; i < sonars.size(); i++)
		//	sonars.get(i).setRange( ranges[i] );
		
		// For each sonar
		for( Sonar s: sonars ) {
			
			// Initialize sonar range
			s.setRange(Sonar.threshold);
			
			// Using robot (x,y,yaw) and sonar (r,theta)
			// Find sonar (X,Y,THETA)
			
			// Polar theta (phi) of sonar = yaw of robot + relative theta of sonar
			phi = validAngle( robo_yaw + s.getTheta() );
			
			// Absolute X and Y values can be found through COSINE and SINE of absolute angle
			x = robo_x + s.getR()*Math.cos( Math.toRadians(phi) );
			y = robo_y + s.getR()*Math.sin( Math.toRadians(phi) );
			
			// Next is the angle that the sonar is pointing in
			t1 = validAngle( s.getAngle() + robo_yaw );
			
			// We need to go the drawing in Map Space, not Robot Space
			// (x1,y1) is the location of the robot
			x1 = mapDisplay.robotToMapX(x);
			y1 = mapDisplay.robotToMapY(y);
			
			// (x2,y2) is the relative vector for (r,T2)
			t2 = t1 + (s.getSpread()/2d);
			x2 = mapDisplay.robotToMapX( robo_x + s.getRange()*Math.cos(Math.toRadians(validAngle(t2))) );
			y2 = mapDisplay.robotToMapY( robo_y + s.getRange()*Math.sin(Math.toRadians(validAngle(t2))) );
			
			// (x3,y3) is the relative vector for (r,T3)
			t3 = t1 - (s.getSpread()/2d);
			x3 = mapDisplay.robotToMapX( robo_x + s.getRange()*Math.cos(Math.toRadians(validAngle(t3))) );
			y3 = mapDisplay.robotToMapY( robo_y + s.getRange()*Math.sin(Math.toRadians(validAngle(t3))) );
			
			// Bounding box wiggle room
			//d = mapDisplay.robotToMapScale( s.getRange() - s.getRange()*Math.cos(Math.toRadians(s.getSpread()/2d)) );			
			d = 20;
			
			// Bounding box
			minx = Math.min(Math.min(x1,x2),x3) - d;
			maxx = Math.max(Math.max(x1,x2),x3) + d;
			miny = Math.min(Math.min(y1,y2),y3) - d;
			maxy = Math.max(Math.max(y1,y2),y3) + d;
			
			for(int i = minx; i <= maxx; i++ ) {
				for(int j = miny; j <= maxy; j++) {
					
					//mapDisplay.setVal(i,j,255);
					
					// Convert i,j to polar r,t
					r = Distance2D(i,x1,j,y1);
					t = -1*Math.toDegrees( Math.atan2(j-y1,i-x1) );
										
					if( t2 > 180 ) {
						if( t < 0 ) {
							t = 360 + t;
						}
					}
					
					// Check if r is within 0->R
					// &&    if t is between t2->t3
					// &&    if that point is an obstacle on the map
					if( r > 0 && r < mapDisplay.robotToMapScale( Sonar.threshold ) ) {
						if( t < t2 && t > t3 ) {
								
							// If the point is an obstacle on the map
							if( mapDisplay.occupied(i,j) ) {
								// If this is the smallest r to date
								if( r < mapDisplay.robotToMapScale(s.getRange()) ) {
									// Mark down this value (Just set the range of the Sonar)
									s.setRange( mapDisplay.mapToRobotScale(r) );
								}
							}
						}
					}		
				}
			}
		}
		
		mapDisplay.repaint();
		//System.out.println(sonars);
		return(sonars);
		
	}
	
	public static void main(String [] args) {
		
		LocalizeHelper l = new LocalizeHelper();
		
	}
		
}
