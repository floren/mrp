import java.io.*;
import java.lang.*;
import java.util.*;

public class Localizer extends Thread {
	
	//private boolean ready = false;
	private boolean dataHasBeenPushed = false;
	private Pose oldPose = new Pose(0d,0d,0d);
	private Pose currentPose;
	private float ranges[] = new float[8];
	private LocalizeHelper helper;
	private long oldTime = 0;
	private long newTime = 0;
	private long counter = 0;	

	public Localizer() {
		
		// Create the Helper Object
		// This should be FAST
		helper = new LocalizeHelper();
		helper.buildRoboStats();
		helper.displayMap();
		System.out.println("Localizer Created.");
	}
	
	public void run() {
		
		System.out.println("Localizer Started.");
		while (true) {
			
			
			// Avoid synchronization issues
			if( dataHasBeenPushed && helper.pointCloudCreated && ranges.length != 0) {

				

				Pose diff = Pose.diffPose( currentPose, oldPose );
				
				// Update the point cloud with the change in Pose
				helper.updateRoboStats( diff );
				oldPose = currentPose;



				// Give the Helper Object sonar values
				// Have the Helper Object get probs for each RoboStat
				helper.traceRoboStats( ranges );

				
				helper.makeNewRoboStats( diff );
				
				//helper.updateMap();
				
				// Threshold based on some probability
				helper.pruneRoboStats();
				
				// Display the valid points on the MapDisplay
				//System.out.println("Display Map");
				helper.updateMap();
				
				// Update the counter
				counter++;
	
				try {
					Thread.sleep(70);
				} catch( Exception e ) {}
				
			} else {
				try {
					Thread.sleep(80);
				} catch( Exception e ) {}
			}
		}
		
	}
	
	public boolean isReady() {
		//return ((counter > 0) && helper.isReady());
		return helper.isReady();
	}
	
	public void push(Pose p, float[] r){
		currentPose = p;
		ranges = r;
		dataHasBeenPushed = true;
	}
	
	public Pose pull() {
		
		//currentPose.setX( currentPose.getX() ); //- 15.5 );
		//currentPose.setY( currentPose.getY() ); //+ 12.0 );
				
		//return currentPose;
		
		if( helper.isReady() ) {
			return helper.getLocation();
		} else {
			System.err.println( "Pulling before localizer is ready." );
			return currentPose;
		}
			
	}

	public static void main(String [] args) {
		Localizer localizer = new Localizer();
	}
	
}
