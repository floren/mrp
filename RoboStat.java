import java.util.*;

public class RoboStat implements Comparable<RoboStat> {
	
	private Pose pose;
	private ArrayList<Sonar> sonars;
	private double prob;
	
	public RoboStat( Pose p ) {
		setPose(p);
	}
	
	public RoboStat( Pose p, ArrayList<Sonar> s ) {
		setPose(p);
		setSonars(s);
	}
	
	public int compareTo( RoboStat r ) {

		if( prob < r.getCurrentProb() ) {
			return 1;
		} else if( prob > r.getCurrentProb() ) {
			return -1;
		} else {
			return 0;
		}
	}
	
	public void setPose( Pose p ) {
		pose = p;
	}
	
	public Pose getPose() {
		return pose;
	}
	
	public void setSonars( ArrayList<Sonar> s ) {
		sonars = s;
	}
	
	public ArrayList<Sonar> getSonars() {
		return sonars;
	}
	
	// This probably shouldn't be used manually
	private void setProb( double p ) {
		prob = p;
	}
	
	public double getCurrentProb() {
		return prob;
	}
	
	public void updateProb( float ranges[] ) {
		
		prob = 0d;
		
		for( int i = 0; i < 8; i++ ) {
			
			prob += sonars.get(i).getProb(ranges[i]);
			
		}

	}
	
}