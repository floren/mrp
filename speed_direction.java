class speed_direction
{
	float speed=0.0f,turnRate=0.0f;
	void setSpeed(float speed,float turnRate)
	{
		this.speed=speed;
		this.turnRate=turnRate;
	}
	float getSpeed()
	{
		return speed;
	}
	float getTurnRate()
	{
		return turnRate;	
	}
}
