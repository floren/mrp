import javaclient2.PlayerClient;
import javaclient2.Position2DInterface;
import javaclient2.SonarInterface;
import javaclient2.structures.PlayerConstants;
import javaclient2.structures.sonar.PlayerSonarData;
import java.util.*;
import java.io.*;

/**
 * Simple hello-world style Player/Stage JavaClient example.
 *
 * @author Gregory Von Pless
 */
public class Retriever {

	/**
	 * Read and print sonars, avoid obstacles.
	 * 
	 * @param args args[0] = server name
	 * 			   args[1] = robot port
	 * 			   these default to localhost and 6665
	 */
	public static void main(String[] args) {
		int port = 6665;
		String server = "localhost";
		
		if (args.length != 3) {
			System.out.println("Invalid args");
			System.exit(0);
		}
		server = args[0];
		port = Integer.parseInt(args[1]);

		ArrayList<Pose> visitMe = new ArrayList<Pose>();
		try {
			Scanner scan = new Scanner(new File(args[2]));
			double x, y;
			for (;;) {
				if (scan.hasNextDouble()) {
					x = scan.nextDouble();
					if (scan.hasNextDouble()) {
						y = scan.nextDouble();
						visitMe.add(new Pose(x, y, 0));
						System.out.println("Adding new destination " + x + ", " + y);
					} else {
						break;
					}
				} else {
					break;
				}
			}
		} catch (Exception e) {}
		
		PlayerClient robot = new PlayerClient(server, port);
		SonarInterface sonar = robot.requestInterfaceSonar(0, 
				PlayerConstants.PLAYER_OPEN_MODE);
		Position2DInterface motor = robot.requestInterfacePosition2D(0, 
				PlayerConstants.PLAYER_OPEN_MODE);
		
		// turn stuff on.  this might not be necessary
		sonar.setSonarPower(1);
		motor.setMotorPower(1);
		
		System.out.println("creating localizer");
		Localizer localizer = new Localizer();		// Make the localizer object
		localizer.start();					// Start the localizer thread
		System.out.println("Created Localizer");
		
		PathPlanner planner = new PathPlanner();		// Make the path planner object
		planner.start();						// Start the planner thread
		
		System.out.println("Created pathplanner");
		RTOA rtoa = new RTOA(planner, visitMe);
		rtoa.start();
		
		System.out.println("Started all threads");
		float turnRate, speed;					// Motor and steering control
		Pose myPose = new Pose();
		float[] ranges;
		
		while (true) {
			
			// read all the data
			robot.readAll();
			
			// don't do anything unless there's data
			if (sonar.isDataReady()) {
				robot.readAll();
				ranges = sonar.getData().getRanges();
				if (ranges.length == 0)
					continue;
				// Give Localizer new sonar values & x,y,theta
				myPose = new Pose( motor.getX(), motor.getY(), motor.getYaw() );
				if (myPose.getTheta() > 180) {
					myPose.setTheta(myPose.getTheta()-360);
				}
				//System.out.println(myPose.print());
				localizer.push( myPose, ranges );
				
				// Ask it if it's ready 
				if(localizer.isReady()) {	// If the Localizer knows where it is, do PP and RTOA
					myPose = localizer.pull();
					planner.updateLocation(myPose);
					rtoa.update_sonars(ranges, myPose);
					speed_direction tmpSpeed = rtoa.getspeed();
					motor.setSpeed(tmpSpeed.getSpeed(),tmpSpeed.getTurnRate());
					
				} else {				// If the Localizer doesn't know where it is, wander
					
					myPose = localizer.pull();
					
					ranges = sonar.getData().getRanges();
					
					// for some reason the first read gets nothing...
					if (ranges.length == 0)
						continue;
					
					myPose = new Pose( motor.getX(), motor.getY(), motor.getYaw() );
					
					// do simple obstacle avoidance
					if (ranges[0] + ranges[1] < ranges[6] + ranges[7])
						turnRate = -20.0f * (float)Math.PI / 180.0f;
					else
						turnRate = 20.0f * (float)Math.PI / 180.0f;
				
					if (ranges[3] < 0.5f)
						speed = 0.0f;
					else
						speed = 0.1f;
				
					// send the command
					motor.setSpeed(speed, turnRate);
				
				}
				
			}
		}
	}
}
